package com.example.myspinner_object_result.Model;

import java.io.Serializable;
import java.util.Objects;

public class Food  implements Serializable {

    private int rating;
    private  String foodName;

    public Food(int rating, String foodName) {
        this.rating = rating;
        this.foodName = foodName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    @Override
    public String toString() {
        return "Food{" +
                "rating=" + rating +
                ", foodName='" + foodName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Food food = (Food) o;
        return rating == food.rating && Objects.equals(foodName, food.foodName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rating, foodName);
    }
}
