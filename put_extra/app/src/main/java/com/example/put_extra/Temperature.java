package com.example.put_extra;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Temperature extends AppCompatActivity {

    EditText editTextTempC;
    TextView textViewTempF;
    Button buttonConvertTemp, btnReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        initialize();
    }

    private void initialize(){


        editTextTempC = findViewById(R.id.editTextTempC);
        textViewTempF = findViewById(R.id.textViewTempF);

        buttonConvertTemp = findViewById(R.id.buttonConvertTemp);
        btnReturn = findViewById(R.id.btnReturn);
    }

    public void OperateTemp(View view){
        int btnID = view.getId();

        switch (btnID){
            case R.id.buttonConvertTemp:
                buttonConvert();
                break;

            case R.id.btnReturn:
                finish();
                break;
        }

    }


    private  void  buttonConvert(){

           float celsius  = Float.parseFloat(editTextTempC.getText().toString());
           double result = ((9.0/5.0)* celsius) +32;

           textViewTempF.setText(String.valueOf(result));
    }
}