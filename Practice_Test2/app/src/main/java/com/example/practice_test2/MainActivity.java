package com.example.practice_test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practice_test2.model.Student;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText studentIdEditText, nameEditText, ageEditText;
    Button btnClear, btnAdd, btnRemove, btnAll;

    //Declare an List
    ArrayList<Student> listOfStudents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {

        listOfStudents = new ArrayList<>();


        //EditView-----------------------------------------------
        studentIdEditText = findViewById(R.id.studentIdEditText);
        nameEditText = findViewById(R.id.nameEditText);
        ageEditText = findViewById(R.id.ageEditText);


        //Button-------------------------------------------
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnAll = findViewById(R.id.btnAll);
        btnAll = findViewById(R.id.btnAll);

        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);

        btnRemove = findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
       switch (view.getId()) {
           case R.id.btnAdd:
               addToArrayList();
               break;
           case R.id.btnAll:
               processShowAll();
               break;
           case R.id.btnClear:
               clearEditText();
               break;
           case R.id.btnRemove:
               processRemove();
               break;
        }
    }

    private void processRemove() {

    }

    private void clearEditText() {
    }



    private void addToArrayList() {

        int studentId = Integer.parseInt(studentIdEditText.getText().toString());
        String name = nameEditText.getText().toString();
        int age = Integer.parseInt(ageEditText.getText().toString());
        Student students = new Student(studentId,name,age);
        listOfStudents.add(students);
        Toast.makeText(this,students.getName()+ " added successfully. Array size: " +
                listOfStudents.size(),Toast.LENGTH_SHORT).show();

       // clearEditText();


    }

    private void processShowAll() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfStudents);

        Intent intent = new Intent(this, ShowResultActivity.class);
        intent.putExtra("intentExtra", bundle);
        startActivity(intent);

    }

    private void clear() {
        finish();
    }


}