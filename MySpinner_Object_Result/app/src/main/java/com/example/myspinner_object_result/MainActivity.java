package com.example.myspinner_object_result;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;

import com.example.myspinner_object_result.Model.Food;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner spinnerMeal;
    ImageView imageViewMeal;
    RatingBar ratingBarMeal;
    Button btnAdd, btnShowAll;

    ArrayList<Food>listOfFood;
    ArrayAdapter<String> mealAdapter;//The string type comes from  Array  listOFMeal[]

    String [] listFood= {"Salmon", "Poutine", "Tacos", "Sushi"};//this array for spinner we are using this array instead of string.xml
    int  imageFood [] = {R.drawable.salmon, R.drawable.poutine, R.drawable.taco, R.drawable.sushi};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initialize();
    }

    private void initialize() {
        //Spinner.................................
        spinnerMeal = findViewById(R.id.spinnerMeal);
        spinnerMeal.setOnItemSelectedListener(this);


        //Button............................
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnShowAll = findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(this);

        //Image.........................................
        imageViewMeal = findViewById(R.id.imageViewMeal);

        //Reference to Rating......................................
        ratingBarMeal = findViewById(R.id.ratingBarMeal);


        //ArrayList..........................................
        listOfFood =new ArrayList<>();

        //AdapterList..................................
        mealAdapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,listFood);
        spinnerMeal.setAdapter(mealAdapter);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAdd:
                addMealRating();
                break;
            case R.id.btnShowAll:
                showAllMealRating();
                break;
        }

    }

    private void showAllMealRating() {
      Bundle bundle = new Bundle();
      bundle.putSerializable("bundleExtra", listOfFood);

        Intent intent = new Intent(this, ShowResultActivity.class);
        intent.putExtra("intentExtra", bundle);
        startActivity(intent);


    }

    private void addMealRating() {
           int rate = (int) ratingBarMeal.getRating();
           String food = spinnerMeal.getSelectedItem().toString();

        Food list = new Food (rate,food);
        listOfFood.add(list);

        ratingBarMeal.setRating(0);


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        int image = imageFood[i];
        imageViewMeal.setImageResource(image);


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}