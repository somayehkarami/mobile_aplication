package com.example.starbuzz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.starbuzz.model.Drink;

public class DrinkActivity extends AppCompatActivity {

    ImageView photo;
    TextView name, description;
    Drink drink;

    public static final String DRINK_ID = "DRINK_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        initialize();
        getMyIntent();
        PopulateData(drink);
    }


    private void initialize() {
        name = findViewById(R.id.name);
        photo = findViewById(R.id.photo);
        description =findViewById(R.id.description);


    }

    private void getMyIntent(){

        //1- Get the drink from the intent
        int drinkId = (int) getIntent().getExtras().get(DRINK_ID);

        drink = Drink.drinks[drinkId];
    }


    private void PopulateData(Drink drink) {
        // 2- Populate the drink name
        name.setText(drink.getName());

        // 3- Populate the drink description
        description.setText(drink.getDescription());

        // 4- Populate the drink image
        photo.setImageResource(drink.getImageResourceId());
        photo.setContentDescription(drink.getName());
    }
}