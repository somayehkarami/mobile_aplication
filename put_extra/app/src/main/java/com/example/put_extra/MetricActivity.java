package com.example.put_extra;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MetricActivity extends AppCompatActivity {

     EditText meterEditText;
     TextView centimeterTextViewValue,kilometerTextViewValue, textViewLastName;

     Button convertBtn, returnBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metric);
        initialize();
        myGetIntent();

    }

    private void initialize(){

        meterEditText =findViewById(R.id.meterEditText);

        centimeterTextViewValue =findViewById(R.id.centimeterTextViewValue);
        kilometerTextViewValue = findViewById(R.id.kilometerTextViewValue);
        textViewLastName =findViewById(R.id.textViewLastName);


        convertBtn = findViewById(R.id.convertBtn);
        returnBtn = findViewById(R.id.returnBtn);

    }


    private void myGetIntent(){
        Intent intent = getIntent();
        String lastName = intent.getStringExtra("LastName");

        textViewLastName.setText(lastName);

    }

    public void convertNew(View view){
        int btnID = view.getId();

        switch (btnID){
            case R.id.convertBtn:
                buttonConvert();
                break;

            case R.id.returnBtn:
                finish();
                break;
        }

    }

    private void buttonConvert(){
        double centimeter = Double.valueOf(meterEditText.getText().toString()) * 100;
        centimeterTextViewValue.setText(String.valueOf(centimeter));


        double kilometer = Double.valueOf(meterEditText.getText().toString()) / 100;
        kilometerTextViewValue.setText(String.valueOf(kilometer));
    }
}