package com.example.put_extra;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editTextLastName;
    Button temperatureBtn, metricBtn, finishBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }

    private void initialize(){

        editTextLastName = findViewById(R.id.editTextLastName);
        temperatureBtn = findViewById(R.id.temperatureBtn);
        metricBtn = findViewById(R.id.metricBtn);
        finishBtn = findViewById(R.id.finishBtn);


    }

    public void operate(View view){

         int  btnID = view.getId();//Any button if pressed by user will be send to view

        switch (btnID){
            case R.id.temperatureBtn:
                temperatureCon();
                break;

            case R.id.metricBtn:
                metricCon();
                break;

            case R.id.finishBtn:
                finish();
                break;

        }

    }

    public void temperatureCon(){

        Intent tempIntent = new Intent(this, Temperature.class);
        startActivity(tempIntent);
    }

    public void metricCon(){
        //read text from EditText
        String lastName = editTextLastName.getText().toString();

        Intent metricIntent = new Intent(this, MetricActivity.class);
        metricIntent.putExtra("LastName",lastName);
        startActivity(metricIntent);
    }


}