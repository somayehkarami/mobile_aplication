package com.example.sum_numbers;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //1-Declare the widget
    EditText editText1, editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }


    // 2-initilize

    private void initialize(){
        //Reference the widget
        editText1 = findViewById(R.id.editTextNumber1);
        editText2 = findViewById(R.id.editTextNumber2);
    }

    //2-onclick
    public void doSum(View view){
        String  editText1String = editText1.getText().toString();
        String  editText2String = editText2.getText().toString();

        //Calculation
        int editText1Int = Integer.parseInt(editText1String);
        int editText2Int = Integer.parseInt(editText2String);

        int sum = editText1Int + editText2Int;

        Toast toast = Toast.makeText(this,String.valueOf(sum),Toast.LENGTH_LONG

        );
        toast.show();



    }
}