package com.example.student_using_object;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.student_using_object.model.Student;

import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText studentIdEditText, nameEditText, ageEditText;
    Button btnClear, btnAdd, btnRemove, btnAll;

    //Declare an List
    ArrayList<Student> listOfStudents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize(){
        //ini of Array
        listOfStudents = new ArrayList<>();


        //EditView-----------------------------------------------
        studentIdEditText = findViewById(R.id.studentIdEditText);
        nameEditText = findViewById(R.id.nameEditText);
        ageEditText = findViewById(R.id.ageEditText);


        //Button-------------------------------------------
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnAll = findViewById(R.id.btnAll);
        btnAll = findViewById(R.id.btnAll);

        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);

        btnRemove = findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

       switch (view.getId()){

           case R.id.btnAdd:
               addToArrayList();
               break;
           case R.id.btnAll:
               processShowAll();
               break;
           case R.id.btnClear:
               clearEditText();
               break;
           case R.id.btnRemove:
               processRemove();
               break;
       }

    }

    private void addToArrayList() {

        int studentID = Integer.parseInt(studentIdEditText.getText().toString());
        String name = nameEditText.getText().toString();
        int age = Integer.parseInt(ageEditText.getText().toString());

            for(Student oneStudent: listOfStudents) {
                if (oneStudent.getStudentID() == studentID) {


                }

            }

            //Add student to arrayList.
            Student student = new Student(studentID, name, age);
            listOfStudents.add(student);

            Toast.makeText(this, student.getName() + " added successfully. Array size: " +
                    listOfStudents.size(), Toast.LENGTH_SHORT).show();

            clearEditText();

        }

    private void processShowAll(){
//why we add Serializable object because  I want to add this object the object is my array
//A marker interface is an interface that has no methods or constants inside it.
// It provides run-time type information about objects, so the compiler and JVM have additional information about the object.like Serializable()
//A marker interface is also called a tagging interface.


        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfStudents);//key-value
        //because listOfStudents is Serializable object we use this method putSerializable()

        Intent intent = new Intent(this, ShowResultActivity.class);
        intent.putExtra("intentExtra", bundle);
        startActivity(intent);


    }
    private void processRemove(){
        int studentID = Integer.parseInt(studentIdEditText.getText().toString());

        boolean find = false;
        Iterator<Student> iterator = listOfStudents.iterator();

        while(!find && iterator.hasNext()){

            Student oneStudent = iterator.next();
            if(oneStudent.getStudentID() == studentID){
                iterator.remove();
                find = true;
            }

        }
        if(find)
            Toast.makeText(this,"The student with the id: "+ studentID +
                    "is deleted successfully", Toast.LENGTH_SHORT).show();

        else
            Toast.makeText(this,"The student with the id : "+
                    studentID + "does not exist.",Toast.LENGTH_SHORT).show();



    }
    private void clearEditText(){

        ageEditText.setText(null);
        nameEditText.setText(null);
        studentIdEditText.setText(null);
    }


}