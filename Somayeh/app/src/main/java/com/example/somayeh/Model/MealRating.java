package com.example.somayeh.Model;

import java.io.Serializable;
import java.util.Objects;

public class MealRating  implements Comparable, Serializable {

    private  String mealName;
    private int rating;

    public MealRating(String mealName, int rating) {
        this.mealName = mealName;
        this.rating = rating;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return
                "Customer Order:  " + mealName +
                        "  Rating:  " + rating+"\n";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MealRating that = (MealRating) o;
        return rating == that.rating && Objects.equals(mealName, that.mealName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mealName, rating);
    }


    @Override
    public int compareTo(Object o) {
        MealRating otherObject =(MealRating) o;
        return mealName.compareTo(otherObject.getMealName());
    }
}
