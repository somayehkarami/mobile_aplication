package com.example.newapp.modle;

import java.util.Date;

public class Customer extends Account{

    private static String name;
    private String family;
    private String phone;
    private int SIN;


    public Customer(int accountNumber, Date openDate, int balance, String family, String phone, int SIN) {
        super(accountNumber, openDate, balance);
        this.family = family;
        this.phone = phone;
        this.SIN = SIN;
    }
}
