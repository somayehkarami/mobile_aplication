package com.example.event_listener;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView textView;
    EditText editText;
    Button submitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize(){
        editText = findViewById(R.id.editText);

        submitBtn = findViewById(R.id.submitBtn);

        textView = findViewById(R.id.textView);

        // Approach 3: Lambda Expression -----------------------------------------------------------
        submitBtn.setOnClickListener((View view) -> {
                    Toast.makeText(MainActivity.this,
                            "Approach 3: Lambda Expression",
                            Toast.LENGTH_LONG).show();

                    getUserInput();
                }
        );
        //------------------------------------------------------------------------------------------

        submitBtn.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        Toast.makeText(MainActivity.this,
                "Approach 4: More common way",
                Toast.LENGTH_LONG).show();

    }

    private void getUserInput() {
        String userInput =  editText.getText().toString();
        textView.setText(userInput);
    }

    public void submit(View view) {

        String myName = editText.getText().toString();
        if (myName.equals("")|| myName.equals(null)){
            myName = "No name";
        }

        textView.setText(myName);
    }
}