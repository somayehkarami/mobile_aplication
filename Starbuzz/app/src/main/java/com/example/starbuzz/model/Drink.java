package com.example.starbuzz.model;

import com.example.starbuzz.R;

public class Drink {

    private String name;
    private String description;
    private int imageResourceId;

    //Each Drink has a name, description, and an image resource


    public Drink(String name, String description, int imageResourceId) {
        this.name = name;
        this.description = description;
        this.imageResourceId = imageResourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    //drinks is an array of Drinks

    public static final Drink[] drinks = {
            new Drink ("Latte","A couple of espresso shots with steamed milk", R.drawable.images),
            new Drink("Cappuccino","A couple of espresso shots with steamed milk", R.drawable.cup )
    };

    @Override
    public String toString() {
        return name;
    }
}
