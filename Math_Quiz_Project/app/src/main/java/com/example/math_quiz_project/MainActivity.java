package com.example.math_quiz_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.math_quiz_project.Model.Score;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE1 = 1;

    EditText editTextInput;
    Button btnGenerate, btnValidate, btnClear, btnScore, btnFinish, btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnDot, btnMinus;
    TextView textViewGenerate, textViewTitle;


    float result = 0;
    float percentage;
    int rightAnswer = 0;
    int wrongAnswer = 0;
    float userAnswer =0;
    String strPercentage;
    String operationResult;
    ArrayList<Score> listOfScore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }


    private void initialize() {

        //ArrayList...................................
        listOfScore = new ArrayList<>();

        //EditText.....................................
        editTextInput = findViewById(R.id.editTextInput);

        //Button........................................
        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);

        btnFinish = findViewById(R.id.btnFinish);
        btnFinish.setOnClickListener(this);

        btnGenerate = findViewById(R.id.btnGenerate);
        btnGenerate.setOnClickListener(this);

        btnScore = findViewById(R.id.btnScore);
        btnScore.setOnClickListener(this);

        btnValidate = findViewById(R.id.btnValidate);
        btnValidate.setOnClickListener(this);

        btn0 = findViewById(R.id.btn0);
        btn0.setOnClickListener(this);

        btn1 = findViewById(R.id.btn1);
        btn1.setOnClickListener(this);

        btn2 = findViewById(R.id.btn2);
        btn2.setOnClickListener(this);

        btn3 = findViewById(R.id.btn3);
        btn3.setOnClickListener(this);

        btn4 = findViewById(R.id.btn4);
        btn4.setOnClickListener(this);

        btn5 = findViewById(R.id.btn5);
        btn5.setOnClickListener(this);

        btn6 = findViewById(R.id.btn6);
        btn6.setOnClickListener(this);

        btn7 = findViewById(R.id.btn7);
        btn7.setOnClickListener(this);

        btn8 = findViewById(R.id.btn8);
        btn8.setOnClickListener(this);

        btn9 = findViewById(R.id.btn9);
        btn9.setOnClickListener(this);

        btnDot = findViewById(R.id.btnDot);
       btnDot.setOnClickListener(this);

        btnMinus = findViewById(R.id.btnMinus);
        btnMinus.setOnClickListener(this);


        //TextView.......................................
        textViewGenerate = findViewById(R.id.textViewGenerate);
        textViewTitle = findViewById(R.id.textViewTitle);
    }

    @Override
    public void onClick(View view) {

      switch (view.getId()){
          case R.id.btnClear:
              clear();
              break;
          case R.id.btnFinish:
              finish();
              break;
          case R.id.btnGenerate:
              generateQuestion();
              break;
          case R.id.btnScore:
              ShowScore();
              break;
          case R.id.btnValidate:
              validateAnswer();
              break;
          case R.id.btn0:
              numberButtons(view);
              break;
          case R.id.btn1:
              numberButtons(view);
              break;
          case R.id.btn2:
              numberButtons(view);
              break;
          case R.id.btn3:
              numberButtons(view);
              break;
          case R.id.btn4:
              numberButtons(view);
              break;
          case R.id.btn5:
              numberButtons(view);
              break;
          case R.id.btn6:
              numberButtons(view);
              break;
          case R.id.btn7:
              numberButtons(view);
              break;
         case R.id.btn8:
              numberButtons(view);
              break;
          case R.id.btn9:
              numberButtons(view);
              break;
          case R.id.btnDot:
              numberButtons(view);
              btnDot.setEnabled(false);
              break;
          case R.id.btnMinus:
              numberButtons(view);
              break;
      }
    }
        private void numberButtons(View view) {
          Button b = (Button) view;

          String userInput = b.getText().toString();
          if (b == btnMinus)  btnMinus.setEnabled(false);
          editTextInput.append(userInput);

    }

    private void clear() {
        editTextInput.getText().clear();
        textViewGenerate.setText("0 + 0");
        btnMinus.setEnabled(true);
    }

    public void generateQuestion() {
        clear();
        Random r = new Random();
        int operand1 = r.nextInt(10);
        int operand2 = r.nextInt(9) + 1;
        int operation = (int) (4 * Math.random() + 1);
        // result = calculateResult(operand1, operand2, operation);
        char operator;
        switch (operation) {
            case 1:
                operator = '+';
                result = operand1 + operand2;
                break;
            case 2:
                operator = '-';
                result = operand1 - operand2;
                break;
            case 3:
                operator = '*';
                result = operand1 * operand2;
                break;
            case 4:
                operator = '/';
                float num = (float)operand1 / operand2;
                String strResult = String.format("%.1f",num);
                result = Float.parseFloat(strResult);
                break;
            default:
                operator = '+';

        }
        operationResult = (operand1 + " "+ operator +" " + operand2 + " ");
        textViewGenerate.setText( operationResult);
    }

    private void validateAnswer() {
        try {
            userAnswer = Float.parseFloat(editTextInput.getText().toString());
            btnValidate.setEnabled(true);

            String strResult;
            //if(editTextInput.getText().toString()==""){

            if (userAnswer == result) {
                strResult = "Your answer is right! ";
                rightAnswer++;
           /* rightAnswer_str = operationResult + "= " + userAnswer + "\n" +  strResult + "\n"
                     + " Right Answer." + "\n" + "Write answer is: " + result + "\n" ;*/
            } else {
                strResult = "Your answer is wrong! ";
                wrongAnswer++;
           /* wrongAnswer_str =  operationResult + "= " + userAnswer + "\n" + strResult + "\n"
                + " Wrong Answer." + "\n" + "Write answer is: " + result + "\n" ;*/
            }

            int total = rightAnswer + wrongAnswer;
            percentage = (rightAnswer * 100) / total;
            strPercentage = percentage + "%";
            Score score = new Score(strResult, total, operationResult, userAnswer, result);
            listOfScore.add(score);
            Toast.makeText(this, strResult, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    private void ShowScore() {

        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfScore);
        Intent intent = new Intent(this,ShowResultActivity.class);
        intent.putExtra("intentExtra", bundle);
        intent.putExtra("percent",strPercentage);
        startActivityForResult(intent,REQUEST_CODE1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE1){
            if(resultCode == RESULT_OK){
                String receivedData = data.getStringExtra("firstName");
                textViewTitle.setText(receivedData);
            }
            else{
                textViewTitle.setText("Cancelled");
            }
        }
    }

}