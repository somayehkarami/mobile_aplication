package com.example.newapp.modle;

import java.util.Date;

public class Account {


    private int accountNumber;
    private Date openDate;
    private int balance;

    public Account(int accountNumber, Date openDate, int balance) {
        this.accountNumber = accountNumber;
        this.openDate = openDate;
        this.balance = balance;
    }

}
