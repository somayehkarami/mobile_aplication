package ca.masoud.event_listener;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView;
    EditText editText;

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {
        textView = findViewById(R.id.textView);
        editText = findViewById(R.id.editText);
        button = findViewById(R.id.button);


        // Approach 1: Create an Object ------------------------------------------------------------
        View.OnClickListener myOnClickListenerObject = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,
                        "Approach 1: Create and Pass an object",
                        Toast.LENGTH_LONG).show();

                getUserInput();
            }
        };
        button.setOnClickListener(myOnClickListenerObject);
        //------------------------------------------------------------------------------------------



        // Approach 2: Anonymous Inner Class -------------------------------------------------------
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,
                        "Approach 2: Anonymous Inner class",
                        Toast.LENGTH_LONG).show();

                getUserInput();
            }
        });
        //------------------------------------------------------------------------------------------



        // Approach 3: Lambda Expression -----------------------------------------------------------
        button.setOnClickListener((View view) -> {
            Toast.makeText(MainActivity.this,
                "Approach 3: Lambda Expression",
                    Toast.LENGTH_LONG).show();

                getUserInput();
            }
        );
        //------------------------------------------------------------------------------------------



        // Approach 4: More common -----------------------------------------------------------------
        button.setOnClickListener(this);


        //------------------------------------------------------------------------------------------
    }


    @Override
    public void onClick(View v) {
        Toast.makeText(MainActivity.this,
                "Approach 4: More common way",
                Toast.LENGTH_LONG).show();
    }

    private void getUserInput() {
        String userInput =  editText.getText().toString();
        textView.setText(userInput);
    }

    public void submit(View view) {

        String myName = editText.getText().toString();
        if (myName.equals("")|| myName.equals(null)){
            myName = "No name";
        }

        textView.setText(myName);
    }
}