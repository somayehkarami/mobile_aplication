package com.example.spinner_rating_bar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.spinner_rating_bar.Model.MealRating;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner spinnerMeal;
    ImageView imageViewMeal;
    RatingBar ratingBarMeal;
    Button btnAdd, btnShowAll;

    String [] listMeal= {"Salmon", "Poutine", "Tacos", "Sushi"};//this array for spinner we are using this array instead of string.xml
    int[] mealPicture = {R.drawable.salmon,R.drawable.poutine ,R.drawable.taco, R.drawable.sushi};

    ArrayList<MealRating> listOfMealRating;
    ArrayAdapter<String>mealAdapter;//The string type comes from  Array  listOFMeal[]



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {

        listOfMealRating = new ArrayList<>();

        spinnerMeal = findViewById(R.id.spinnerMeal);
        spinnerMeal.setOnItemSelectedListener(this);

        //Reference to ratings
        ratingBarMeal = findViewById(R.id.ratingBarMeal);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnShowAll = findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(this);

        imageViewMeal =findViewById(R.id.imageViewMeal);

        mealAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listMeal);//3 arguments 1-where 2-how 3-what
        spinnerMeal.setAdapter(mealAdapter);//set the adapter for the spinner

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnAdd:
                addMealRating();
                break;
            case R.id.btnShowAll:
                showAllMealRating();
                break;

        }

    }

    private void showAllMealRating() {
       Collections.sort(listOfMealRating);

       StringBuilder sb= new StringBuilder("");

       for(MealRating oneMealRating: listOfMealRating){
           sb.append(oneMealRating).append("\n");
       }

        Toast.makeText(this,sb.toString(),Toast.LENGTH_SHORT).show();
    }

    private void addMealRating() {

        String meal = spinnerMeal.getSelectedItem().toString();

        //Read ratingBar..............................
        int rating = (int) ratingBarMeal.getRating(); //getRating is floating type and i cast it ti integer
        //.............................................


        //Create new Object and add it to our model array

        MealRating mealRating = new MealRating(meal,rating);
        listOfMealRating.add(mealRating);

        //Reset rating bar for next time
        ratingBarMeal.setRating(0);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        int image = mealPicture[i];
        imageViewMeal.setImageResource(image);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}