package com.example.somayeh_test2.modle;

import java.util.Date;

public class Customer {

    private static String name;
    private String family;
    private String phone;
    private int SIN;

    public Customer(String name, String family, String phone, int SIN) {
        this.name = name;
        this.family = family;
        this.phone = phone;
        this.SIN = SIN;
    }

    public static String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getSIN() {
        return SIN;
    }

    public void setSIN(int SIN) {
        this.SIN = SIN;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", family='" + family + '\'' +
                ", phone='" + phone + '\'' +
                ", SIN=" + SIN +
                '}';
    }


    public static class Account extends Customer {

        private int accountNumber;
        private Date openDate;
        private int balance;


        public Account(String name, String family, String phone, int SIN, int accountNumber, Date openDate, int balance) {
            super(name, family, phone, SIN);
            this.accountNumber = accountNumber;
            this.openDate = openDate;
            this.balance = balance;
        }


        public int getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(int accountNumber) {
            this.accountNumber = accountNumber;
        }

        public Date getOpenDate() {
            return openDate;
        }

        public void setOpenDate(Date openDate) {
            this.openDate = openDate;
        }

        public int getBalance() {
            return balance;
        }

        public void setBalance(int balance) {
            this.balance = balance;
        }

        @Override
        public String toString() {
            return "Account{" +
                    "accountNumber=" + accountNumber +
                    ", openDate=" + openDate +
                    ", balance=" + balance +
                    '}';
        }


    }

    public static Account[] accounts = {
           new  Account("Jhon", "Smith", "5143333333", 1, 01,
                    new Date("10/12/2018"), 5),
            new Account("Alex", "Bugg", "5145555555", 2, 02,
                    new Date("10/12/2020"), 10)
    };
}