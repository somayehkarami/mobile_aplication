package com.example.change_spinner_data_source;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    Spinner spinnerDays;
    Button btnAssignColors, btnDisplayTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {
        spinnerDays = findViewById(R.id.spinnerDays);
        spinnerDays.setOnItemSelectedListener(this);

        btnAssignColors = findViewById(R.id.btnAssignColors);
        btnAssignColors.setOnClickListener(this);

        btnDisplayTextView =findViewById(R.id.btnDisplayTextView);
        btnDisplayTextView.setOnClickListener(this);



    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //'adapterView' The AdapterView where the click happened
        // 'i' is the index of selected item in the spinner
        //'l' is the row id of the item that was clicked


        TextView textview = (TextView) view;
        String selectedTextView  = textview.getText().toString();

        Toast.makeText(this,"The selected item is "+ selectedTextView +
                "\n Position is " +i+
                "\n Row id is "+l,
                Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    //View.OnClickListener Interface
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnDisplayTextView:

                View selectedView = spinnerDays.getSelectedView();
                TextView selectedTextView = (TextView) selectedView;
                String selectedText = selectedTextView.getText().toString();
                Toast.makeText(this, "You select : " + selectedText, Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnAssignColors:
                //ArrayAdapter is a class and  createFromResource is method from this class
                // =>"this" means when you want to present data
                //=>R.array.myColors, what is array you want to assign to  spinner
                //R.layout.support_simple_spinner_dropdown_item==> predefined layout for  the spinner


                ArrayAdapter adapter = ArrayAdapter.createFromResource
                        (this, R.array.myColors, R.layout.support_simple_spinner_dropdown_item);


                spinnerDays.setAdapter(adapter);
                break;

                //Adapter => we cant directly assign the array to view in layout that's why we need adapter

        }

    }
}