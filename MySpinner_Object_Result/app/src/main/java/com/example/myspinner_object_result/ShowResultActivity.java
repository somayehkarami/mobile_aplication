package com.example.myspinner_object_result;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.myspinner_object_result.Model.Food;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowResultActivity extends AppCompatActivity {

    TextView textViewResult;
    ArrayList<Food> listOfMeal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);

        initialize();
    }

    private void initialize() {

        textViewResult = findViewById(R.id.textViewResult);


        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("intentExtra");
        Serializable bundleLostOfFood = bundle.getSerializable("bundleExtra");
        listOfMeal = (ArrayList<Food>) bundleLostOfFood;


        showListOfMeal(listOfMeal);
    }

    private void showListOfMeal(ArrayList<Food> listOfMeal) {

        String str ="";
        for(Food oneMeal : listOfMeal){
            str = str + oneMeal;
        }
        textViewResult.setText(str);
    }
}