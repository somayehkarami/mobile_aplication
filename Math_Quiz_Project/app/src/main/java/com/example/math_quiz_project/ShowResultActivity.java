package com.example.math_quiz_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.math_quiz_project.Model.Score;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class ShowResultActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener ,View.OnClickListener {


    String intentPercent;
    EditText editTextScore, editTextName;
    ArrayList<Score> listOfScore;
    Spinner spinnerScore;
    String selectedSpinnerText;
    ListView listView;
    Button btnBack;
    ArrayAdapter<Score> listAdapterViewList;

    //ArrayAdapter<Score> listAdapterObject;
    ArrayAdapter<String> listAdapter;//The string type comes from  Array  listOFMeal[]

    String[] listAction = {"All", "Right Answer", "Wrong Answer", "SortByAscending", "SortByDescending"};//this array for spinner we are using this array instead of string.xml

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);

        initialize();

    }

    private void initialize() {

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        //EditView.....................................
        editTextScore = findViewById(R.id.editTextScore);
        editTextName = findViewById(R.id.editTextName);
        //Spinner......................................
        spinnerScore = findViewById(R.id.spinnerScore);
        spinnerScore.setOnItemSelectedListener(this);

        listView = findViewById(R.id.listview);

        //Adapter......................................
        listAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listAction);
        spinnerScore.setAdapter(listAdapter);


        //Intent.......................................
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("intentExtra");
        Serializable bundleListOfScore = bundle.getSerializable("bundleExtra");
        intentPercent = intent.getStringExtra("percent");
        listOfScore = (ArrayList<Score>)bundleListOfScore;
        System.out.println(listOfScore.toString());
        //showListOfScore(listOfScore);

    }


    public void goBack() {
        String name = editTextName.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("firstName", name);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        TextView spinnerTEXT = (TextView) view;
        selectedSpinnerText = spinnerTEXT.getText().toString();
        String selectedItem = spinnerScore.getSelectedItem().toString();

        Toast.makeText(this, "Selected spinner: " + selectedSpinnerText,
                Toast.LENGTH_SHORT).show();
        //Switch..................................
        switch (selectedSpinnerText) {

            case "All":
                showAll();
                break;
            case "Right Answer":
                showRightAnswer();
                break;
            case "Wrong Answer":
                showWrongAnswer();
                break;
            case "SortByAscending":
                sort(1);
                break;
            case "SortByDescending":
                sort(2);
                break;
        }
    }

    private void showRightAnswer() {
        ArrayAdapter<String> answerArrayAdapter;
        ArrayList<String> rightList = new ArrayList<>();

        for (Score oneScore : listOfScore) {
            if ((oneScore.getUserAnswer()) == oneScore.getResult()) {
                rightList.add(String.valueOf(oneScore));

                answerArrayAdapter = new
                        ArrayAdapter<String>(
                        this,
                        android.R.layout.simple_list_item_1,
                        rightList);

                // 3- Assign the Adapter to the list view
                listView.setAdapter(answerArrayAdapter);

            }
        }
    }

    private void sort(int i) {
        if (i == 1) {
            Collections.sort(listOfScore);
            listAdapterViewList = new
                    ArrayAdapter<>(
                    this,
                    android.R.layout.simple_list_item_1,
                    listOfScore);

            // 3- Assign the Adapter to the list view
            listView.setAdapter(listAdapterViewList);
        } else {
            Collections.sort(listOfScore, Collections.reverseOrder());
            listAdapterViewList = new
                    ArrayAdapter<>(
                    this,
                    android.R.layout.simple_list_item_1,
                    listOfScore);

            // 3- Assign the Adapter to the list view
            listView.setAdapter(listAdapterViewList);
        }
    }

    private void showAll() {
        // 2- Create an Adapter for ListView
        listAdapterViewList = new
                ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                listOfScore);

        // 3- Assign the Adapter to the list view
        listView.setAdapter(listAdapterViewList);
        editTextScore.setText(intentPercent);

    }

    private void showWrongAnswer() {
        ArrayAdapter<String> answerArrayAdapter;
        ArrayList<String> wrongList = new ArrayList<>();

        for (Score oneScore : listOfScore) {
            if ((oneScore.getUserAnswer()) != oneScore.getResult()) {
                wrongList.add(String.valueOf(oneScore));

                answerArrayAdapter = new
                        ArrayAdapter<String>(
                        this,
                        android.R.layout.simple_list_item_1,
                       wrongList);

                // 3- Assign the Adapter to the list view
                listView.setAdapter(answerArrayAdapter);

            }
        }


    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void onClick(View view) {
        goBack();
    }
}



