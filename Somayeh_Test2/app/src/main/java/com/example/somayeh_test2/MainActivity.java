package com.example.somayeh_test2;

import java.io.Serializable;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.somayeh_test2.modle.Customer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    Button btnAdd;
    ArrayList<Account> listOfCustomer;
    ArrayAdapter<Account> listAdapterViewList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeListView();
    }


    private void initializeListView() {

        // 1- Initialize ListView
        ListView listView = findViewById(R.id.list_customer);
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);


        // 2- Create an Adapter for ListView
        ArrayAdapter<Customer.Account> listAdapter;
        listAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1, Customer.Account.accounts);

        listView.setAdapter(listAdapter);

        //When I add this block my program will crush inside the try catch

       /* try {
            listAdapterViewList = new
                    ArrayAdapter<>(
                    this,
                    android.R.layout.simple_list_item_1,
                    listOfCustomer);

            // 3- Assign the Adapter to the list view
            listView.setAdapter(listAdapterViewList);
            Intent intent = getIntent();
            Bundle bundle = intent.getBundleExtra("intentExtra");
            Serializable bundledListOfStudents = bundle.getSerializable("bundleExtra");
            listOfCustomer = (ArrayList<Account>) bundledListOfStudents;


        } catch (Exception e) {

        }*/
    }






    @Override
    public void onClick(View view) {

       // Bundle bundle = new Bundle();
       // bundle.putSerializable("bundleExtra",Customer.Account.accounts);
        Intent intent = new Intent(this, DetailActivity.class);
       // intent.putExtra("intentExtra", bundle);
        startActivity(intent);

    }




}