package com.example.multiple_activites;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Activiy2 extends AppCompatActivity {

    EditText editTextResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activiy2);
        initialize();
        myGetIntent();


    }

    private void initialize(){
        editTextResult = findViewById(R.id.editTextResult);

    }

    //An Activity represents a single screen in an app. You can start a new instance of an Activity
    // by passing an Intent to startActivity().
    // The Intent describes the activity to start and carries any necessary data.
    private void myGetIntent(){
        float result = getIntent().getFloatExtra("result",0f);

        String strResult = String.valueOf(result);
        editTextResult.setText(strResult);
    }

    public void goBack(View view){
        finish();
    }
}