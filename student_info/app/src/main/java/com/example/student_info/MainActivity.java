package com.example.student_info;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    //1-Declare the widget
    EditText editText1, editText2, editText3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }


    private void initialize(){
        //return types of editText1,2,3 are EditText
        //findViewById(R.id.editTextId);return type is EditText and is a generic method
        //findViewById() comes from AppCompactActivity
        editText1 = findViewById(R.id.editTextId);
        editText2 = findViewById(R.id.editTextName);
        editText3 = findViewById(R.id.editTextAge);

    }

    public void showInfo(View view){
        //getText() return object type of  Editable that s why I call toString()
        String  editText1String = editText1.getText().toString();
        String  editText2String = editText2.getText().toString();
        String  editText3String = editText3.getText().toString();

        //String info =String.format("My id is: %s", editText1String, "The name is: %s ",editText2String+"The age is:%s ",editText3String);
        String info ="The id is:  " +editText1String+ "\n"+ "The name is:  "+editText2String+ "\n"+"The age is: " +editText3String;

        //return type of makeText is Toast  with 3 arguments
        //class Toast and static method
        //this where you want to show the toast
        Toast toast = Toast.makeText(this,info,Toast.LENGTH_LONG);

        toast.show();//we have to use the object in hip

    }
}