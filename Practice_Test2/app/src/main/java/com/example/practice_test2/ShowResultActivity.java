package com.example.practice_test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.practice_test2.model.Student;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowResultActivity extends AppCompatActivity {


    ListView listView;
    ArrayList<Student> listOfStudents;
    ArrayAdapter<Student> listAdapterViewList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ShowResultActivity);

        initialize();
    }

    private void initialize() {
        listView = findViewById(R.id.listview);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("intentExtra");
        Serializable bundledListOfStudents = bundle.getSerializable("bundleExtra");

        listOfStudents = (ArrayList<Student>) bundledListOfStudents;


        showAll();

    }

    private void showAll() {
        // 2- Create an Adapter for ListView
        listAdapterViewList  = new ArrayAdapter<>
                (this, android.R.layout.simple_list_item_1,listOfStudents);

        listView.setAdapter(listAdapterViewList);

    }
}