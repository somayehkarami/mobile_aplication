package com.example.somayeh;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.somayeh.Model.MealRating;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ShowResultActivity extends AppCompatActivity {

    RadioGroup radioGroup;
    EditText editTextName;
    TextView textViewResult;

    ArrayList<MealRating> listOfMeal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);
        initialize();

    }

    private void initialize() {

        radioGroup = findViewById(R.id.radioGroup);
        editTextName = findViewById(R.id.editTextName);
        textViewResult = findViewById(R.id.textViewResult);

        //Intent...........................................
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("intentExtra");
        Serializable bundleListOfFood = bundle.getSerializable("bundleExtra");
        listOfMeal = (ArrayList<MealRating>) bundleListOfFood;
        showListOfMeal(listOfMeal);
    }

    public void goBack(View view){
        finish();
        String name = editTextName.getText().toString();
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("firstName", name);
        startActivity(intent);
    }

    public void runMe(View view){
        int selectedRadioBtn = radioGroup.getCheckedRadioButtonId();


        switch (selectedRadioBtn){
            case R.id.rdOneStar:
                showOneStar();
                break;
            case R.id.rdTwoStars:
                showTwoStars();
                break;
            case R.id.rdThreeStars:
                showThreeStars();
                break;
            case R.id.rdDesanding:
                SortByDescending();
                break;
            case R.id.rdAscending:
                sortByAscending();
                break;
        }

    }

    private void sortByAscending() {
        Collections.sort(listOfMeal);
        String str ="";
        for(MealRating oneMeal : listOfMeal){
            str = str + oneMeal;
        }
        textViewResult.setText(str);
    }

    private void SortByDescending () {

        Collections.sort(listOfMeal, Collections.reverseOrder());
        String str ="";
        for(MealRating oneMeal : listOfMeal){
            str = str + oneMeal;
        }
        textViewResult.setText(str);

    }


    private void showThreeStars() {

        String str ="";
        for(MealRating oneMeal : listOfMeal){
           if(oneMeal.getRating() ==3){
               str = str + oneMeal;
           }
            textViewResult.setText(str);
        }

    }

    private void showTwoStars() {

        String str ="";
        for(MealRating oneMeal : listOfMeal){
            if(oneMeal.getRating() == 2){
                str = str + oneMeal;
            }
            textViewResult.setText(str);
        }
    }

    private void showOneStar(){

        String str ="";
        for(MealRating oneMeal : listOfMeal){
            if(oneMeal.getRating() == 1){
                str = str + oneMeal;
            }
            textViewResult.setText(str);
        }
    }


    private void showListOfMeal(ArrayList<MealRating> listOfMeal) {

        String str ="";
        for(MealRating oneMeal : listOfMeal){
            str = str + oneMeal;
        }
        textViewResult.setText(str);

    }
    

}