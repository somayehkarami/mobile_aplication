package com.example.somayeh;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.somayeh.Model.MealRating;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner spinnerMeal;
    ImageView imageViewMeal;
    RatingBar ratingBarMeal;
    Button btnAdd, btnShowAll, btnMeal, btnSalad;
    TextView textViewTitle;


    String [] listMeal= {"Salmon", "Poutine", "Tacos", "Sushi"};
    int[] mealPicture = {R.drawable.salmon,R.drawable.poutine ,R.drawable.taco, R.drawable.sushi};
    int[] saladPicture ={R.drawable.greensalad, R.drawable.salad, R.drawable.chopped};

    ArrayList<MealRating> listOfMealRating;
    ArrayAdapter<String> mealAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        myGetIntent();


    }

    private void myGetIntent(){
        Intent intent = getIntent();
        String lastName = intent.getStringExtra("firstName");
        textViewTitle.setText(lastName);

    }

    private void initialize() {

        listOfMealRating = new ArrayList<>();

        spinnerMeal = findViewById(R.id.spinnerMeal);
        spinnerMeal.setOnItemSelectedListener(this);

        //TextView.......................................
        textViewTitle = findViewById(R.id.textViewTitle);

        ratingBarMeal = findViewById(R.id.ratingBarMeal);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnShowAll = findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(this);

        btnMeal = findViewById(R.id.btnMeal);
        btnMeal.setOnClickListener(this);

        btnSalad = findViewById(R.id.btnSalad);
        btnSalad.setOnClickListener(this);

        imageViewMeal =findViewById(R.id.imageViewMeal);

        mealAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listMeal);//3 arguments 1-where 2-how 3-what
        spinnerMeal.setAdapter(mealAdapter);//set the adapter for the spinner
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAdd:
                addMealRating();
                break;
            case R.id.btnShowAll:
                showAllMealRating();
                break;
            case R.id.btnSalad:
                ArrayAdapter adapter = ArrayAdapter.createFromResource
                        (this, R.array.mySalad, R.layout.support_simple_spinner_dropdown_item);
                spinnerMeal.setAdapter(adapter);
                break;
            case R.id.btnMeal:
                mealAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listMeal);//3 arguments 1-where 2-how 3-what
                spinnerMeal.setAdapter(mealAdapter);
                break;
        }

    }

    private void addMealRating() {

        String meal = spinnerMeal.getSelectedItem().toString();

        //Read ratingBar..............................
        int rating = (int) ratingBarMeal.getRating();
        MealRating mealRating = new MealRating(meal,rating);
        listOfMealRating.add(mealRating);
        ratingBarMeal.setRating(0);

    }

    private void showAllMealRating() {
        Collections.sort(listOfMealRating);
        StringBuilder sb= new StringBuilder("");
        for(MealRating oneMealRating: listOfMealRating){
            sb.append(oneMealRating).append("\n");
        }
        //Toast
        Toast.makeText(this,sb.toString(),Toast.LENGTH_SHORT).show();

        //Bundle
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfMealRating);
        //Intent
        Intent intent = new Intent(this, ShowResultActivity.class);
        intent.putExtra("intentExtra", bundle);
        startActivity(intent);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        int image = mealPicture[i];
        imageViewMeal.setImageResource(image);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}