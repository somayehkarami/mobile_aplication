package com.example.somayeh_test2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.somayeh_test2.modle.Customer;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextAccountNumber, editTextOpenDate, editTextBalance, editTextName,
            editTextFamily, editTextPhone, editTextSIN;

    Button  btnAdd, btnFind, btnRemove, btnClear;
    ArrayList<Customer.Account> listOfCustomers;
    public static final String ACCOUNT_ID = "ACCOUNT_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initialize();
    }

    private void initialize() {


        listOfCustomers = new ArrayList<>();
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnFind = findViewById(R.id.btnFind);
        btnFind.setOnClickListener(this);

        btnRemove = findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);

        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);


        editTextAccountNumber = findViewById(R.id.editTextAccountNumber);
        editTextBalance = findViewById(R.id.editTextBalance);
        editTextFamily = findViewById(R.id.editTextFamily);
        editTextName = findViewById(R.id.editTextName);
        editTextOpenDate = findViewById(R.id.editTextOpenDate);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextSIN = findViewById(R.id.editTextSIN);

       /* Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("intentExtra");
        Serializable bundledListOfCustomers = bundle.getSerializable("bundleExtra");
        Customer.Account.accounts = (Customer.Account[]) bundledListOfCustomers;*/

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnAdd:
                addToArrayList();
                break;
            case R.id.btnClear:
                clear();
                break;
            case R.id.btnRemove:
                processRemove();
                break;
            case R.id.btnFind:
                find();
                break;
            case R.id.btnShowAll:
                showAll();
                break;
            case R.id.btnLoad:
                loade();
                break;
        }

    }

    private void loade() {
    }

    private void showAll() {

        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfCustomers);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("intentExtra", bundle);
        startActivity(intent);
    }

    private void find() {
        int accountNumber = Integer.parseInt(editTextAccountNumber.getText().toString());

        boolean find = false;
        Iterator<Customer.Account> iterator = listOfCustomers.iterator();

        while (!find && iterator.hasNext()) {
            Customer.Account oneCustomer = iterator.next();
            if (oneCustomer.getAccountNumber() == accountNumber) {
                find = true;
            }
        }
        if (find)
            Toast.makeText(this, "The customer with the accountNumber: " +
                            accountNumber + " exist",
                    Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "The customer with the accountNumber: " +
                            accountNumber + " does not exist.",
                    Toast.LENGTH_LONG).show();


    }

    private void processRemove() {

        int accountNumber = Integer.parseInt(editTextAccountNumber.getText().toString());

        boolean find = false;
        Iterator<Customer.Account> iterator = listOfCustomers.iterator();

        while (!find && iterator.hasNext()) {
            Customer.Account oneCustomer = iterator.next();
            if (oneCustomer.getAccountNumber() == accountNumber) {
                iterator.remove();
                find = true;
            }
        }
    }


    private void addToArrayList() {

        int accountNumber =Integer.parseInt(editTextAccountNumber.getText().toString());
        DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        Date openDate = null;
        try {
            openDate = format.parse(editTextOpenDate.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int balance =Integer.parseInt(editTextBalance.getText().toString());
        int sin =Integer.parseInt(editTextSIN.getText().toString());
        String name = editTextName.getText().toString();
        String family = editTextFamily.getText().toString();
        String phone = editTextPhone.getText().toString();


       Customer.Account customers = new Customer.Account(name,family,phone,sin,accountNumber,openDate,balance);
       listOfCustomers.add(customers);

        Toast.makeText(this,
                Customer.Account.getName() +
                        " added successfully. Array size: " +
                        listOfCustomers.size(),
                Toast.LENGTH_SHORT).show();

        clear();

    }

    private void clear() {

        editTextAccountNumber.setText(null);
        editTextBalance.setText(null);
        editTextFamily.setText(null);
        editTextName.setText(null);
        editTextOpenDate.setText(null);
        editTextPhone.setText(null);
        editTextSIN.setText(null);


    }
}