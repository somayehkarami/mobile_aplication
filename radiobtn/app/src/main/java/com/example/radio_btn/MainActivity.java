package com.example.radio_btn;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    RadioGroup radioGroup ;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }


    // 2-initilize

    private void initialize(){
        //Reference the widget
        radioGroup = findViewById(R.id.radioGroup);
        imageView = findViewById(R.id.imageView);
    }


    public void runMe(View view){
        int selectedRadioBtn = radioGroup.getCheckedRadioButtonId();


        switch (selectedRadioBtn){
            case R.id.radioButton1:
                imageView.setImageResource(R.drawable.patrick_star);
                break;

            case R.id.radioButton2:
                imageView.setImageResource(R.drawable.patric);
                break;

            case R.id.radioButton3:
                imageView.setImageResource(R.drawable.sponge_bob);
                break;

        }

    }

}