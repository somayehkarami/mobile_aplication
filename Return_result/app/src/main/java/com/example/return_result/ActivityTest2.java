package com.example.return_result;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityTest2 extends AppCompatActivity implements View.OnClickListener {

    Button test2_OK_Btn,test2_Cancel_Btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);

        initialize();
    }

    private void initialize() {

        test2_OK_Btn =findViewById(R.id.test2_OK_Btn);
        test2_OK_Btn.setOnClickListener(this);

        test2_Cancel_Btn =findViewById(R.id.test2_Cancel_Btn);
        test2_Cancel_Btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.test2_OK_Btn:
                goOk();
                break;

            case R.id.test2_Cancel_Btn:
                goCancel();
                break;
        }
    }

    private void goCancel() {
        String strResult ="Operation Canceled";

        Intent intent = new Intent();
        intent.putExtra("Cancel_tag" , strResult);

        setResult(RESULT_CANCELED,intent);
        finish();
    }

    private void goOk() {

        String strResult ="Ok result from test2";

        Intent intent = new Intent();
        intent.putExtra("return_result_from_test2" , strResult);

        setResult(RESULT_OK,intent);
        finish();
    }


}