package com.example.student_using_object;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.student_using_object.model.Student;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowResultActivity extends AppCompatActivity {

    TextView textViewResult;
    ArrayList<Student> listOfStudent;
    /* we don't new the arrayList because is going to be a new arrayList without any students
    * I want to pass the array to here
     */
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);

        initialize();
    }
    
    private void initialize(){
        
        textViewResult = findViewById(R.id.textViewResult);
        
        
        Intent intent =  getIntent();
        Bundle bundle = intent.getBundleExtra("intentExtra");
        Serializable bundledListOfStudents = bundle.getSerializable("bundleExtra");
        
        listOfStudent = (ArrayList<Student>)bundledListOfStudents;
        
        showListOfStudents(listOfStudent);
    }

    private void showListOfStudents(ArrayList<Student> listOfStudent) {

        String str ="";
        for (Student oneStudent : listOfStudent){
            str = str + oneStudent;

        }

        textViewResult.setText(str);
    }
}