package com.example.math_quiz_project.Model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Comparator;

public class Score implements Serializable , Comparable{

    private  String allAnswer;
    private  int total;
    private  String  operation;
    private  float userAnswer;
    private  float result;

    public Score(String allAnswer, int total, String operation, float userAnswer,float result){
        this.allAnswer = allAnswer;
        this.total = total;
        this.operation = operation;
        this.userAnswer = userAnswer;
        this.result= result;

    }


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getAllAnswer() {
        return allAnswer;
    }

    public void setAllAnswer(String allAnswer) {
        this.allAnswer = allAnswer;
    }

    public float getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(float userAnswer) {
        this.userAnswer = userAnswer;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    @Override
    public String toString() {
        DecimalFormat decForm1 = new DecimalFormat();
        DecimalFormat decForm2 = new DecimalFormat("0.#");
        decForm1.setDecimalSeparatorAlwaysShown(false);
        decForm1.setGroupingUsed(false);
        return

                  "The Operation is: " + operation + " =  " + decForm2.format(userAnswer) +
                          "\n"+ allAnswer +"\n"+ "Write Answer is: " +decForm2.format(result )+ "\n";
    }

    @Override
    public int compareTo(Object o) {
        Score otherObject =(Score) o;
        return  allAnswer.compareTo(otherObject.getAllAnswer());
    }
}
